# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 13:54:59 2021

@author: Bljeona
"""

import matplotlib.image as mpimg
from sklearn import cluster
import numpy as np
import matplotlib.pyplot as plt

imageNew = mpimg.imread('C:\\Users\\Bljeona\\Documents\\rusu_lv_2019_2020\\LV5\\resources\\example_grayscale.png')
    
X = imageNew.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=10,n_init=1)
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
img_compressed = np.choose(labels, values)
img_compressed.shape = imageNew.shape

plt.figure(1)
plt.imshow(imageNew,  cmap='gray')

plt.figure(2)
plt.imshow(img_compressed,  cmap='gray')
