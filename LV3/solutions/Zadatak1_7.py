import pandas as pd
#1.1zad
mtcars = pd.read_csv(r"C:\Users\student\Desktop\lv3\rusu_lv_2019_2020\LV3\resources\mtcars.csv")
print(mtcars.nlargest(5, 'mpg').car)

#1.2zad

osamCilindara=mtcars.query("cyl==8")

print(osamCilindara.nsmallest(3,'mpg').car)

#1.3zad

sestCilindara=mtcars.query("cyl==6")

print(sestCilindara["mpg"].mean())

#1.4zad

first=mtcars.query("cyl==4")
second=first.query('wt > 2.000 & wt<2.200')

print(second["mpg"].mean)

#1.5zad

rucni=mtcars.query("am==0")
automatik=mtcars.query("am==1")

print(rucni.shape[0])
print(automatik.shape[0])

#1.6zad

overHP=automatik.query("hp>100")

print(overHP.shape[0])

#1.7zad

print(mtcars.wt*1000)