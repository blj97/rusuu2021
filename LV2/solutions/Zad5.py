import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

plt.figure()
cars=pd.read_csv("../resources/mtcars.csv")
plt.scatter(cars.hp, cars.mpg, c=cars.wt)
plt.colorbar()
plt.ylabel("MPG")
plt.xlabel("HP")
print(" Prosjecna potrosnja je: %f\n"%np.average(cars.mpg),
      "Minimalna potrosnja je: %f\n"%min(cars.mpg),
      "Maksimalna potrosnja je: %f\n"%max(cars.mpg))
plt.show()
