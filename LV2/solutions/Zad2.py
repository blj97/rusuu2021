import numpy as np
import matplotlib.pyplot as plt

A = np.random.randint(1, 7, 100)
bins = np.linspace(1, 6, 20)
plt.hist(A, bins, color='pink')
plt.show()
