import re

fh = open("../resources/mbox-short.txt")
minA = []
jednoA = []
nemaA = []
broj = []
slova = []

for line in fh:
    line = line.rstrip()
    mail = re.findall('[a-zA-Z0-9._]+@[a-zA-Z0-9._]+', line)
    if mail:
        if mail[0].count('a')>=1:
            minA.append(mail[0].split('@')[0])
        if mail[0].count('a')==1:
            jednoA.append(mail[0].split('@')[0])
        if mail[0].count('a')==0:
            nemaA.append(mail[0].split('@')[0])
        if re.findall('[0-9]+', mail[0]):
            broj.append(mail[0].split('@')[0])
        if mail[0].islower():
            slova.append(mail[0].split('@')[0])

print(minA)
print(jednoA)
print(nemaA)
print(broj)
print(slova)
